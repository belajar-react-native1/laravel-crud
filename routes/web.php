<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function() {
    return "Halo";
});

//CRUD

//Create
//Form input data create
Route::get('/cast/create', 'KategoriController@create');

Route::post('/cast', 'CastController@store')

//Read
//semua data
Route::get('/cast', 'CastController@index');
//berdasarkan id
Route::get('/cast/{id}', 'CastController@show');

//Update
Route::get('/cast/{id}/edit', 'CastController@edit');
//update data by id
Route::get('/cast/{id}', 'CastController@update');

//Delete
Route::get('/cast/{id}', 'CastController@destroy');
